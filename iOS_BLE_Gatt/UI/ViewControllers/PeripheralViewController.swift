//
//  PeripheralViewController.swift
//  iOS_BLE_Gatt
//
//  Created by Padma Pravallika Gottumukkala on 21/12/20.
//

import UIKit
import CoreBluetooth

class PeripheralViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, CBCentralManagerDelegate, BlePeripheralDelegate {

    // MARK: UI Elements
    
    @IBOutlet weak var advertisedNameLabel: UILabel!
    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var rssiLabel: UILabel!
    @IBOutlet weak var gattTableView: UITableView!
    
    // Gatt Table Cell Reuse Identifier
    let gattCellReuseIdentifier = "GattTableViewCell"
    
    // Segue
    let segueIdentifier = "LoadCharacteristicViewSegue"
    
    
    // MARK: Connected Peripheral Properties
    
    // Central Manager
    var centralManager:CBCentralManager!
    
    // connected Peripheral
    var blePeripheral:BlePeripheral!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Will connect to \(blePeripheral.peripheral.identifier.uuidString)")
        blePeripheral.delegate = self
        centralManager.delegate = self
        centralManager.connect(blePeripheral.peripheral)
    }
    
    
    
    // MARK: BlePeripheralDelegate
    
    /* Did Discover Characteristics will update UI */
    func blePerihperal(discoveredCharacteristics characteristics: [CBCharacteristic], forService: CBService, blePeripheral: BlePeripheral) {
        self.blePeripheral = blePeripheral
        print("reloading table with new characteristics")
        print(blePeripheral.gattProfile)
        gattTableView.reloadData()
    }
    
    /* RSSI will update UI */
    func blePeripheral(readRssi rssi: NSNumber, blePeripheral: BlePeripheral) {
        rssiLabel.text = rssi.stringValue
    }
    
    
    
    // MARK: CBCentralManagerDelegate code
    
    
    /* Connected Peripheral will update UI */
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected Peripheral: \(String(describing: peripheral.name))")
        advertisedNameLabel.text = blePeripheral.advertisedName
        identifierLabel.text = blePeripheral.peripheral.identifier.uuidString
        
        blePeripheral.connected(peripheral: peripheral)
    }
    
    /* Peripheral connection has Failed */
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("failed to connect")
        print(error.debugDescription)
    }
    
    /* Disconnecting Peripheral */
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected Peripheral: \(peripheral.name)")
        dismiss(animated: true, completion: nil)
    }
    
    /* Bluetooth  state  changed */
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Central Manager updated: checking state")
        switch (central.state) {
        case .poweredOn:
            print("bluetooth on")
        default:
            print("bluetooth unavailable")
        }
    }
    
    
    
    
    // MARK: UITableViewDataSource
    
    /* Number Of Rows In Section */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("returning num rows in section")
        if section < blePeripheral.gattProfile.count {
            if let characteristics = blePeripheral.gattProfile[section].characteristics {
                return characteristics.count
            }
        }
        return 0
    }
    
    /* Cell For Row At IndexPath */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("returning table cell")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: gattCellReuseIdentifier, for: indexPath) as! GattTableViewCell
        
        let section = indexPath.section
        let row = indexPath.row
        if section < blePeripheral.gattProfile.count {
            if let characteristics = blePeripheral.gattProfile[section].characteristics {
                if row < characteristics.count {
                    cell.renderCharacteristic(characteristic: characteristics[row])
                }
            }
        }
        return cell
    }
    
    /* Number of Service Sections */
    func numberOfSections(in tableView: UITableView) -> Int {
        print("returning number of sections")
        print(blePeripheral)
        print(blePeripheral.gattProfile)
        return blePeripheral.gattProfile.count
    }
    
    /* Header Title */
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        print("returning title at section \(section)")
        if section < blePeripheral.gattProfile.count {
            return blePeripheral.gattProfile[section].uuid.uuidString
        }
        return nil
    }
    
    
    /* Selected Row */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRow = indexPath.row
        print("Selected Row: \(selectedRow)")
    }
    
    
    
    // MARK: Navigation
    
    /* Pushing to Other Screen */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("leaving view - disconnecting from peripheral")
        
        if let indexPath = gattTableView.indexPathForSelectedRow {
            let selectedSection = indexPath.section
            let selectedRow = indexPath.row
            
            let characteristicViewController = segue.destination as! CharacteristicViewController
            
            if selectedSection < blePeripheral.gattProfile.count {
                let service = blePeripheral.gattProfile[selectedSection]
                
                if let characteristics = blePeripheral.gattProfile[selectedSection].characteristics {
                    
                    if selectedRow < characteristics.count {
                        // populate next UIView with necessary information
                        characteristicViewController.centralManager = centralManager
                        characteristicViewController.blePeripheral = blePeripheral
                        characteristicViewController.connectedService = service
                        characteristicViewController.connectedCharacteristic = characteristics[selectedRow]
                    }
                    
                }
            }
            
            gattTableView.deselectRow(at: indexPath, animated: true)
            
        } else {
            if let peripheral = blePeripheral.peripheral {
                centralManager.cancelPeripheralConnection(peripheral)
            }
        }
        
    }
    
    
    
    
    
}

