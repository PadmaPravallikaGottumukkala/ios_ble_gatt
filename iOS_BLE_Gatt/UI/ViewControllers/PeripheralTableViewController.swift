//
//  PeripheralTableViewController.swift
//  iOS_BLE_Gatt
//
//  Created by Padma Pravallika Gottumukkala on 21/12/20.
//

import UIKit
import CoreBluetooth

class PeripheralTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, CBCentralManagerDelegate {

    // MARK: UI Elements
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet var peripheralsDisplayViewController: UITableView!
    
    // Default unknown advertisement name
    let unknownAdvertisedName = "(UNMARKED)"
    
    // PeripheralTableViewCell reuse identifier
    let peripheralCellReuseIdentifier = "PeripheralTableViewCell"
    
    
    // MARK: Scan Properties
    
    // total scan time
    let scanTimeout_s = 5; // seconds
    
    // current countdown
    var scanCountdown = 0
    
    // scan timer
    var scanTimer:Timer!
    
    // Central Bluetooth Manager
    var centralManager: CBCentralManager!


    
    // discovered peripherals
    var blePeripherals = [BlePeripheral]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Initializing central manager")
        self.centralManager = CBCentralManager(delegate: self, queue: nil,options: nil)
    }
    
    
    /*Start and Stop Button*/
    @IBAction func onScanButtonClicked(_ sender: UIButton) {
        print("scan button clicked")
        // if scanning
        if centralManager.isScanning {
            stopBleScan()
        } else {
            startBleScan()
        }
    }
    
    
    /* Scan for Bluetooth peripherals */
    func startBleScan() {
        scanButton.setTitle("Stop", for: .normal)
        //blePeripherals.removeAll()
        //tableView.reloadData()
        print ("discovering devices")
        scanCountdown = scanTimeout_s
        scanTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateScanCounter), userInfo: nil, repeats: true)
        
        if let centralManager = centralManager {
            centralManager.scanForPeripherals(withServices: nil, options: nil)
        }
        
    }
    
    /* Stop scanning for Bluetooth Peripherals */
    func stopBleScan() {
        if let centralManager = centralManager {
            centralManager.stopScan()
        }
        scanTimer.invalidate()
        scanCountdown = 0
        scanButton.setTitle("Start", for: .normal)
    }
    
    
    /* Update the scan countdown timer */
    
    @objc func updateScanCounter() {
        //you code, this is an example
        if scanCountdown > 0 {
            print("\(scanCountdown) seconds until Ble Scan ends")
            scanCountdown -= 1
        } else {
            stopBleScan()
        }
    }
    
    
    
    // MARK:  CBCentralManagerDelegate Functions
    
    /* New Peripheral discovered */
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        //print("Discovered \(peripheral.name)")
        print("Discovered \(peripheral.identifier.uuidString) (\(peripheral.name))")
        
        
        
        // check if this peripheral has already been discovered
        var peripheralFound = false
        for blePeripheral in blePeripherals {
            if blePeripheral.peripheral.identifier == peripheral.identifier {
                peripheralFound = true
                break
            }
        }
        
        
        // don't duplicate discovered devices
        if !peripheralFound {
            
            print(advertisementData)
            var advertisedName = unknownAdvertisedName
            if let alternateName = BlePeripheral.getNameFromAdvertisementData(advertisementData: advertisementData) {
                advertisedName = alternateName
            } else {
                if let peripheralName = peripheral.name {
                    advertisedName = peripheralName
                }
            }
            
            // don't display peripherals that can't be connected to
            if BlePeripheral.isConnectable(advertisementData: advertisementData) {
                let blePeripheral = BlePeripheral(delegate: nil, peripheral: peripheral)
                blePeripheral.rssi = RSSI
                blePeripheral.advertisedName = advertisedName
                blePeripherals.append(blePeripheral)
                peripheralsDisplayViewController.reloadData()
            }
            
        }
        
    }
    
    
    
    /* Bluetooth radio state changed */
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Central Manager updated: checking state")
        
        switch (central.state) {
        case .poweredOn:
            print("BLE Hardware powered on and ready")
            scanButton.isEnabled = true
        default:
            print("Bluetooth unavailable")
        }
    }
    
    
    
    
    // MARK: - Table view data source
   
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blePeripherals.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("setting up table cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: peripheralCellReuseIdentifier, for: indexPath) as! PeripheralTableViewCell
        
        // fetch the appropritae peripheral for the data source layout
        let peripheral = blePeripherals[indexPath.row]
        cell.renderPeripheral(peripheral)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        stopBleScan()
        
        let selectedRow = indexPath.row
        print("Row: \(selectedRow)")
        
        print(blePeripherals[selectedRow])
    }
    
    
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let peripheralViewController = segue.destination as! PeripheralViewController
        if let selectedIndexPath = peripheralsDisplayViewController.indexPathForSelectedRow {
            let selectedRow = selectedIndexPath.row
            
            if selectedRow < blePeripherals.count {
                // prepare next UIView
                peripheralViewController.centralManager = centralManager
                peripheralViewController.blePeripheral = blePeripherals[selectedRow]
            }
            
            peripheralsDisplayViewController.deselectRow(at: selectedIndexPath, animated: true)
        }
        
        
    }
    
    
    
    
}
